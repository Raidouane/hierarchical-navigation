﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HierarchicalNavigation
{
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
        }

        async void HandleNavigateToCatPage(object sender, EventArgs args)
        {
            await Navigation.PushAsync(new CatPage(Name.Text));
        }
    }
}
