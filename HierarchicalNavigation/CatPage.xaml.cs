﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HierarchicalNavigation
{
    public partial class CatPage : ContentPage
    {
        public CatPage(string name)
        {
            InitializeComponent();
            nameLabel.Text = "Hello " + name;
        }
    }
}
